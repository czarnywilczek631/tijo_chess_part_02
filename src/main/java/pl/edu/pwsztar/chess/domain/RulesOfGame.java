package pl.edu.pwsztar.chess.domain;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import pl.edu.pwsztar.chess.controller.ChessApiController;

public interface RulesOfGame {

    /**
     * Metoda zwraca true, tylko gdy przejscie z polozenia
     * source na destination w jednym ruchu jest zgodne
     * z zasadami gry w szachy
     */
    boolean isCorrectMove(Point source, Point destination);

    static final Logger LOGGER = LoggerFactory.getLogger(ChessApiController.class);


    class Bishop implements RulesOfGame {

        @Override
        public boolean isCorrectMove(Point source, Point destination) {
            if(source.getX() == destination.getX() && source.getY() == destination.getY()) {
                return false;
            }

            return Math.abs(destination.getX() - source.getX()) ==
                    Math.abs(destination.getY() - source.getY());
        }
    }

    class Knight implements RulesOfGame {

        @Override
        public boolean isCorrectMove(Point source, Point destination) {
            if(source.getX() == destination.getX() && source.getY() == destination.getY()) {
                LOGGER.info("*** Fail: equal points");
                return false;
            }

            if (Math.abs(destination.getX() - source.getX()) == 2){
                return (Math.abs(destination.getY() - source.getY()) == 1);
            }

            if (Math.abs(destination.getX() - source.getX()) == 1){
                LOGGER.info("*** Correct1: {}", Math.abs(destination.getX() - source.getX()));
                return (Math.abs(destination.getY() - source.getY()) == 2);
            }

            LOGGER.info("*** Fail: end of function");
            return false;
        }
    }

    class Rook implements RulesOfGame {

        @Override
        public boolean isCorrectMove(Point source, Point destination) {
            if(source.getX() == destination.getX() && source.getY() == destination.getY()) {
                LOGGER.info("*** Fail: equal points");
                return false;
            }

            return (source.getX() == destination.getX() || source.getY() == destination.getY());
        }
    }

    class Pawn implements RulesOfGame {

        @Override
        public boolean isCorrectMove(Point source, Point destination) {
            if(source.getX() == destination.getX() && source.getY() == destination.getY()) {
                LOGGER.info("*** Fail: equal points");
                return false;
            }

            if (source.getY() == 2 && Math.abs(destination.getY() - source.getY()) == 2 && source.getX() == destination.getX()) {return true;}

            return (source.getX() == destination.getX() && destination.getY() - source.getY() == 1);
        }
    }

    class Queen implements RulesOfGame {

        @Override
        public boolean isCorrectMove(Point source, Point destination) {
            Rook rook = new Rook();
            Bishop bishop = new Bishop();
            return (bishop.isCorrectMove(source, destination) || rook.isCorrectMove(source, destination));
        }
    }

    class King implements RulesOfGame {

        @Override
        public boolean isCorrectMove(Point source, Point destination) {
            if(source.getX() == destination.getX() && source.getY() == destination.getY()) {
                LOGGER.info("*** Fail: equal points");
                return false;
            }

            return (Math.abs(destination.getY() - source.getY()) <= 1 && Math.abs(destination.getX() - source.getX()) <= 1);
        }
    }

    // TODO: Prosze dokonczyc implementacje kolejnych figur szachowych: Knight, King, Queen, Rook, Pawn
    // TODO: Prosze stosowac zaproponowane nazwy klas !!!
    // TODO: Kazda klasa powinna implementowac interfejs RulesOfGame
}
