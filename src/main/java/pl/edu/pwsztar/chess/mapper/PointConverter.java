package pl.edu.pwsztar.chess.mapper;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import pl.edu.pwsztar.chess.controller.ChessApiController;
import pl.edu.pwsztar.chess.domain.Point;

public class PointConverter implements Converter<Point, String> {

    private static final Logger LOGGER = LoggerFactory.getLogger(ChessApiController.class);


    @Override
    public Point convert(String from) {
        String template = "abcdefgh";
        int x = template.indexOf(from.charAt(0));
        LOGGER.info("*** X : {}", x);
        int y = Integer.parseInt(String.valueOf(from.charAt(2)));
        LOGGER.info("*** Y : {}", y);
        return new Point(x, y);
    }
}
