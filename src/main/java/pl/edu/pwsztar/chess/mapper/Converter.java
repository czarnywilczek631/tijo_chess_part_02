package pl.edu.pwsztar.chess.mapper;

public interface Converter<T, F> {
    T convert(F from);
}
