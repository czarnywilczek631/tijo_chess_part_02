package pl.edu.pwsztar.chess.service.impl;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import pl.edu.pwsztar.chess.controller.ChessApiController;
import pl.edu.pwsztar.chess.domain.Point;
import pl.edu.pwsztar.chess.domain.RulesOfGame;
import pl.edu.pwsztar.chess.dto.FigureMoveDto;
import pl.edu.pwsztar.chess.mapper.PointConverter;
import pl.edu.pwsztar.chess.service.ChessService;

@Service
@Transactional
public class ChessServiceImpl implements ChessService {
    private PointConverter pointConverter = new PointConverter();
    private RulesOfGame bishop;
    private RulesOfGame knight;
    private RulesOfGame rook;
    private RulesOfGame pawn;
    private RulesOfGame queen;
    private RulesOfGame king;
    // ...

    static final Logger LOGGER = LoggerFactory.getLogger(ChessApiController.class);
    public ChessServiceImpl() {
        bishop = new RulesOfGame.Bishop();
        knight = new RulesOfGame.Knight();
        rook = new RulesOfGame.Rook();
        pawn = new RulesOfGame.Pawn();
        queen = new RulesOfGame.Queen();
        king = new RulesOfGame.King();
        // ...
    }

    public boolean isCorrectMove(FigureMoveDto figureMoveDto) {

        // refaktoryzacja?
        switch (figureMoveDto.getType()) {
            case BISHOP:
                // wywolaj konwerter punktow oraz popraw ponizszy kod
                LOGGER.info("*** Bishop");
                return bishop.isCorrectMove(pointConverter.convert(figureMoveDto.getSource()), pointConverter.convert(figureMoveDto.getDestination()));
            case KNIGHT:
                // wywolaj konwerter punktow oraz popraw ponizszy kod
                LOGGER.info("*** Knight");
                return knight.isCorrectMove(pointConverter.convert(figureMoveDto.getSource()), pointConverter.convert(figureMoveDto.getDestination()));
            case ROOK:
                LOGGER.info("*** Rook");
                return rook.isCorrectMove(pointConverter.convert(figureMoveDto.getSource()), pointConverter.convert(figureMoveDto.getDestination()));
            case PAWN:
                LOGGER.info("*** Pawn");
                return pawn.isCorrectMove(pointConverter.convert(figureMoveDto.getSource()), pointConverter.convert(figureMoveDto.getDestination()));
            case QUEEN:
                LOGGER.info("*** Queen");
                return queen.isCorrectMove(pointConverter.convert(figureMoveDto.getSource()), pointConverter.convert(figureMoveDto.getDestination()));
            case KING:
                LOGGER.info("*** King");
                return king.isCorrectMove(pointConverter.convert(figureMoveDto.getSource()), pointConverter.convert(figureMoveDto.getDestination()));
        }

        return false;
    }
}
